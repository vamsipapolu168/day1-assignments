public class ContactSearch {
    
    public static List<Contact> searchForContacts(String name,String pCode){
        
        List<Contact> cont=[SELECT ID,Name FROM Contact WHERE lastName =:name AND MailingPostalCode =:pCode];
        return cont;
   }

}