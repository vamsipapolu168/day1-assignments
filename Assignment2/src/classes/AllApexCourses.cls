public class AllApexCourses {
 public Static List<List< SObject>> searchAllApexCourses(){
        List<List<sObject>> searchList = [FIND 'Apex*' IN ALL FIELDS 
                   RETURNING APEX_Course__c(Id,APEX_Course_Name__c, APEX_Credits__c)];
         System.debug(searchList);
        return searchList;
     }
}