public class StaticClass {
    private static integer num=1;
    static final integer PRIVATE_INT_CONST;
    static final integer PRIVATE_INT_CONST2=200;
    public static integer calculate(){
        return 2+7;
    }
    static{
        PRIVATE_INT_CONST =calculate();
    }
    public void incrementNum(){
        num++;
    }
    public static void showNum(){
        System.debug('value of :'+num);
        System.debug('value of PRIVATE_INT_CONST:'+PRIVATE_INT_CONST);
        System.debug('value of PRIVATE_INT_CONST2:'+PRIVATE_INT_CONST2);
    }
}