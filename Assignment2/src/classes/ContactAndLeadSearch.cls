public class ContactAndLeadSearch {
    public static List<List< SObject>> searchContactsAndLeads(String str){
        List<List< SObject>> searchList = [FIND :str IN ALL FIELDS RETURNING Contact(FirstName,LastName,Department),Lead(FirstName,LastName)];
        return searchList;
    }

}